#' Select biggest flood events in a flow time series
#'
#' @inheritParams get_flood_pool
#' @param freq_target Trequency of flood for defining the threshold (Number per year)
#'
#' @return See value returned by [get_flood_pool].
#' @export
#'
#' @examples
select_floods <- function(Q,
                          n,
                          freq_target = 1,
                          nb_target = round(lubridate::time_length(difftime(tail(Q[,1], 1), Q[1,1]), "years") * freq_target),
                          inter_min_steps = 30,
                          tol = 0.5,
                          extend = 2,
                          normalization = normalization,
                          ...) {
  # Select the floods
  # Threshold definition
  fun_nb_events <- function(threshold, nb_target) {
    n <- nrow(extract_events(Q, threshold = threshold, inter_min_steps = inter_min_steps, peak = FALSE)) - nb_target
    message("threshold=", threshold, " n-target=", n)
    n
  }
  threshold <- uniroot(fun_nb_events, interval =  quantile(Q[, 2], probs = c(0.5, 1)),
                       nb_target = nb_target, tol = tol)$root
  # Extract n greatest floods
  flood_pool <-
    get_flood_pool(Q,
                   threshold = threshold,
                   inter_min_steps = inter_min_steps,
                   n = n,
                   extend = extend,
                   normalization = normalization,
                   ...)
  return(flood_pool)
}
