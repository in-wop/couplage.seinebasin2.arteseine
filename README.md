
# couplage.seinebasin2.arteseine

<!-- badges: start -->
<!-- badges: end -->

The Inrae HBAN and G-EAU units produced a semi-distributed hydrological model of the Seine catchment integrating the 4 reservoirs managed by the EPTB Seine Grands Lacs. The model was calibrated on the flows of the catchment influenced by the reservoirs and was then forced by climate projections in de-influenced flow.

The flows modelled by these climate projections will be used to model flood events on the Telemac-2D model of the Seine implemented by Artelia.

The purpose of this package is to knit a report producing and containing:

- The coupling of the outputs of the semi-distributed hydrological model with the inputs of the Telemac 2 model;
- The selection relevant flood events to be modelled from the events observed in the climate projections;
- The conversion of the time series of the selected events into Telemac format.

Translated with www.DeepL.com/Translator (free version)

## Installation

- Download the source code at https://forgemia.inra.fr/in-wop/couplage.seinebasin2.arteseine/-/archive/main/couplage.seinebasin2.arteseine-main.zip
- Install the package:

``` r
# install.packages(devtools)
devtools::install_local()
```

## Configuration

Create a "config.yml"" file in the root directory of the package source with at least the following content:

```yml
default:
  couplage:
    write_results: TRUE
    path: "Put here the local path for output data"
```

`write_results: FALSE` tells the package to not write time series in the folder `path` 
during the build or the execution of the report.
`write_results: TRUE` triggers the write of the time series.

This configuration is loaded with the call to: `cfg <- loadConfig()` and it is 
completed with the default configuration of the **seinebasin2** package. 
Configuration parameters of the **seinebasin2** package can be overwritten by redefining 
them in this configuration file.

## Knit of the report

Open the file "couplage.Rmd" and knit it with RStudio!
