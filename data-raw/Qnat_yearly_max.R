library(seinebasin2)
devtools::load_all(".")
cfg <- seinebasin2::loadConfig()

maxAndWrite <- function(filename, df) {
  dfMaxYear <- airGR::SeriesAggreg(df, "%Y", rep("max", ncol(df) - 1))
  dfMaxYear[, 1] <- lubridate::year(dfMaxYear[, 1])
  dir.create("outputs", showWarnings = FALSE)
  readr::write_tsv(dfMaxYear,
                   file.path("outputs", paste0(filename, ".tsv")))
}

df <- readQsim(cfg$Qnat$path, "historical", "data")
maxAndWrite("in-wop_historical_sim_Qnat", df)

df <- seinebasin2::loadHydratecDB(cfg = cfg)$Q
maxAndWrite("sgl_hydratec_Qnat", df)
